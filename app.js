const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const bodyParser = require('body-parser');
require('dotenv').config();
const session = require('express-session');
const { ExpressOIDC } = require('@okta/oidc-middleware');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const oidc = new ExpressOIDC({
    issuer: process.env.ORG_URL,
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
    appBaseUrl: process.env.HOST_URL,
    scope: process.env.SCOPE
});

app.use(session({
    secret: 'this should be secure',
    resave: true,
    saveUninitialized: false
}));

app.use(oidc.router);

app.get('/', (req, res) => {
    if (req.userContext) {
        let username = req.userContext.userinfo.name;
        res.render('index.ejs', { username : username });

  } else {
    res.send('Please <a href="/login">login</a>');
  }
});

app.get('/protected', oidc.ensureAuthenticated(), (req, res) => {
  res.send('Top Secret');
});

app.get('/chat', oidc.ensureAuthenticated(), (req, res) => {
    let username = JSON.stringify(req.userContext.userinfo.name);
    res.render('chat.ejs', { username : username });
});

app.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

oidc.on('ready', () => {
  app.listen(3000, () => console.log('app started'));
});

oidc.on('error', err => {
    console.log('Unable to configure ExpressOIDC', err);
});

io.sockets.on('connection', function(socket) {
    socket.on('username', function(username) {
        socket.username = username;
        io.emit('is_online', '🔵 <i>' + socket.username + ' has joined the chat..</i>');
    });

    socket.on('disconnect', function(username) {
        io.emit('is_online', '🔴 <i>' + socket.username + ' left the chat..</i>');
    });

    socket.on('chat_message', function(message) {
        io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + message);
    });

});

const server = http.listen(8080, function() {
    console.log('listening on *:8080');
});
